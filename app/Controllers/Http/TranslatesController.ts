import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class TranslatesController {

    public async translate ({request, response}:HttpContextContract){
        var translate = require('node-google-translate-skidz');

        await translate({
        source: request.input ('source'),
        target: request.input ('target'),
        text: request.input ('text')
        },
        function(result) {
            const trans = result.translation
            return response.status(200).json(trans)
        });

    }
}
