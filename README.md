# translate-app

# You need to have XAMPP or similar app to run the app locally, and Postman App or Visual Studio Code app with downloaded Thunder Client Extension

# How to run the app:
# 1. Unzip the source code
# 2. Go to translate-app folder
# 3. Open XAMPP and turn on Apache and MySQL
# 4. Run 'node ace serve --watch' in your terminal
# 5. Open Postman App or Thunder Client Extension on Visual Studio Code App, make a new request, then enter url 'localhost:3333/api/v1/translate'
# 6. Choose POST method, go to Body, choose form encode
# 7. Write 'text' on 'name' parameter
# 8. For 'value' parameter, open source.txt file, then copy the text inside
# 9. Back to Postman App, paste the text on 'value' parameter
# 10. Repeat the step no. 7 but this time write 'source' on 'name' parameter
# 11. Write 'id' on 'value' parameter as the source text was written on Indonesian
# 12. Repeat the step no. 10 and 11, but write 'target' and 'en' for 'name' and 'value' parameters respectively as the taget language is english for this time
# 13. Click Send, and the result will appear on the response panel
# 14. Check the result and compare it to the text inside target.txt file
# 15. If the result was same then the app was succesfully work!
# 16. You can change the source text, source languange, and target language by yourself, just check on https://cloud.google.com/translate/docs/languages for supported language